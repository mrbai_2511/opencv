#include "cvideoplayer.h"

#define STRDEF QStringLiteral

CVideoPlayer::CVideoPlayer(QWidget *parent, bool bServer)
	: QWidget(parent),
	m_bServer(bServer)
{
	ui.setupUi(this);
	this->setWindowTitle(STRDEF("Qt+Opencv摄像头读取视频保存以及回放播放器 WX:MrBai_2511"));
	connect(ui.listWidget, &QListWidget::itemDoubleClicked, this, &CVideoPlayer::slot_dbclicked);
	
	connect(ui.slider_voice, &QSlider::valueChanged, [&](int value) {
		m_nVoice = value;
		m_player->setVolume(m_nVoice);
	});

	connect(ui.slider_play, &QSlider::valueChanged, [&](int value) {
		if (ui.slider_play->isSliderDown())	{
			m_player->setPosition(value * m_player->duration() / 100);
		}
	});

	m_player = new QMediaPlayer(this);
    connect(m_player, SIGNAL(positionChanged(qint64)), this, SLOT(slot_positionChanged(qint64)));
	connect(m_player, SIGNAL(durationChanged(qint64)), this, SLOT(slot_getduration(qint64)));

	ui.widget->setAttribute(Qt::WA_OpaquePaintEvent);

	//ui.widget->setAspectRatioMode(Qt::IgnoreAspectRatio);
	m_player->setVideoOutput(ui.widget);
	ReflushDir();
	ui.slider_voice->setRange(0, 100);
	ui.slider_voice->setValue(50);

	ui.slider_play->setRange(0, 100);
	ui.slider_play->setValue(0);
}

CVideoPlayer::~CVideoPlayer()
{

}

void CVideoPlayer::ReflushDir()
{
	QString strFilePath = QCoreApplication::applicationDirPath();
	if (m_bServer) {
		strFilePath += "/servervideo/" ;
	}
	else {
		strFilePath += "/clientvideo/";
	}
	QDir currentDir(strFilePath);
	//QStringList mp4Files = currentDir.entryList(QStringList() << "*.mp4", QDir::Files);
	QStringList mp4Files = currentDir.entryList(QStringList() << "*.avi", QDir::Files);

	foreach(const QString & file, mp4Files) {
		qDebug() << file;
	}
	m_vecVideos = mp4Files;
	UpdateToUI();
}

void CVideoPlayer::UpdateToUI()
{
	ui.listWidget->clear();

	QString strText;
	QListWidgetItem* item = nullptr;
	for (int i=0;i<m_vecVideos.size();i++)
	{
		strText = m_vecVideos[i];
		item = new QListWidgetItem();
		item->setText(strText);
		ui.listWidget->addItem(item);
	}
}

void CVideoPlayer::on_btn_play_clicked()
{
	ui.widget->setUpdatesEnabled(false);
	m_player->play();
}

void CVideoPlayer::on_btn_pause_clicked()
{
	m_player->pause();
}

void CVideoPlayer::on_btn_stop_clicked()
{
	ui.widget->setUpdatesEnabled(true);
	m_player->stop();
}

void CVideoPlayer::slot_dbclicked(QListWidgetItem* item)
{
	if (item == nullptr) return;
	int index = ui.listWidget->currentRow();
	if (index < 0) return;

	ui.lab_currentplay_name->setText(item->text());
	QString strFilePath = QCoreApplication::applicationDirPath();
	if (m_bServer) {
		strFilePath = strFilePath + "/servervideo/" + item->text();
	}
	else {
		strFilePath = strFilePath +  "/clientvideo/" + item->text();
	}
	m_player->stop();
	m_player->setMedia(QUrl::fromLocalFile(strFilePath));
	m_player->setVolume(m_nVoice);
	ui.widget->setUpdatesEnabled(false);
	m_player->play();
}

void CVideoPlayer::slot_positionChanged(qint64 pos)
{
	// 如果正在手动滑动条，则直接退出
	if (ui.slider_play->isSliderDown())	{
		return;
	}
	int nSize = m_player->duration();
	if (nSize == 0) return;
	ui.slider_play->setSliderPosition(100 * pos / m_player->duration());

	QTime currentTime(0, 0, 0, 0); 
	currentTime = currentTime.addMSecs(m_player->position()); 
	m_strCurrentFormattedTime = currentTime.toString("hh::mm:ss"); 
	ui.lab_play_stat->setText(m_strCurrentFormattedTime + " / " + m_strTotalFormattedTime);
}

void CVideoPlayer::slot_getduration(qint64 duration)
{
	QTime totalTime = QTime(0, 0, 0, 0);
	totalTime = totalTime.addMSecs(duration); 
	m_strTotalFormattedTime = totalTime.toString("hh::mm:ss"); 
	ui.lab_play_stat->setText(m_strCurrentFormattedTime + " / " + m_strTotalFormattedTime);
}


// player->setPlaybackRate(value * 2.0 / 8.0);//设置倍速播放
