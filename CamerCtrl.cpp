#include "CamerCtrl.h"
#include <QCoreApplication>
#include <QDateTime>

CamerCtrl::CamerCtrl(QObject *parent)
	: QObject(parent)
{
	// 开启一个定时器从视频或者摄像头读取帧数据
	m_pTimer = new QTimer(this);
	m_pTimer->setInterval(20);// 默认50fps
	connect(m_pTimer, &QTimer::timeout, this, &CamerCtrl::slot_timerout);

	connect(this, &CamerCtrl::signal_MatImage, this, &CamerCtrl::slot_recordvideo);
}

CamerCtrl::~CamerCtrl()
{
	if (m_cap.isOpened()) {
		m_cap.release();
	}
}

/*
	检查设备是否已打开
*/
bool CamerCtrl::CheckDeviceOpend()
{
	bool bRet = m_cap.isOpened();
	if (bRet == false) {
		qDebug() << "CamerCtrl device is not opened.";
	}
	return bRet;
}

/*
	设置属性。
*/
void CamerCtrl::SetCamerSetting(int proid, double value)
{
	if (CheckDeviceOpend()) {
		if (proid == cv::CAP_PROP_FPS) {
			if (m_currentFPS != value) {
				m_pTimer->stop();
				m_pTimer->setInterval(1000 / m_currentFPS);
				m_pTimer->start();
			}
			m_currentFPS = value;
		}
		m_cap.set(proid, value);
	}
}

/*
	打开摄像头
*/
bool CamerCtrl::OpenCamer(int index)
{
	StopCamera();
	m_cap.open(index, cv::CAP_DSHOW);

	m_currentFPS = m_cap.get(cv::CAP_PROP_FPS);
	if (!m_cap.isOpened())	{
		return false;
	}
	if (m_currentFPS == 0) m_currentFPS = 30;  // 默认30fps
	m_pTimer->setInterval(1000 / m_currentFPS);
	m_pTimer->start();
	return true;
}

/*
	这个是为了测试用的 打开视频
*/
bool CamerCtrl::OpenCamer(QString strVideoPath)
{
	StopCamera();
	m_cap.open(strVideoPath.toStdString().c_str());
	m_currentFPS = m_cap.get(cv::CAP_PROP_FPS);
	if (!m_cap.isOpened()) {
		return false;
	}
	if (m_currentFPS == 0) m_currentFPS = 30;  // 默认30fps
	m_pTimer->setInterval(1000 / m_currentFPS);
	m_pTimer->start();
	return true;
}

/*
	关闭视频或者摄像头
*/
void CamerCtrl::StopCamera()
{
	if (m_cap.isOpened())
	{
		m_pTimer->stop();
		m_cap.release();
	}
}

/*
	开始录制视频
*/
bool CamerCtrl::StartRecordVideo()
{
	if (CheckDeviceOpend() == false) return false;

	// 获取一下是否保存的视频位置
	QString strFilePath = QCoreApplication::applicationDirPath();
	strFilePath += "/servervideo/";
	QDir dir(strFilePath);
	if (dir.exists() == false) {
		dir.mkpath(strFilePath);
	}

	// 打开录制功能
	if (!m_bRecordVideo && !m_videorecord.isOpened()) {
		int width = m_cap.get(cv::CAP_PROP_FRAME_WIDTH);
		int height = m_cap.get(cv::CAP_PROP_FRAME_HEIGHT);
		cv::Size _size = cv::Size(width, height);
		//double frameRate = m_cap.get(cv::CAP_PROP_FPS);
		//QString video_name = QString("%1.mp4").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm-ss"));
		QString video_name = QString("%1.avi").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm-ss"));
		strFilePath += video_name;
		//m_videorecord.open(strFilePath.toStdString(), cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), frameRate, _size, true);
		m_videorecord.open(strFilePath.toStdString(), cv::VideoWriter::fourcc('I', '4', '2', '0'), m_currentFPS, _size, true);
		if (m_videorecord.isOpened()) {
			m_bRecordVideo = true;
			return true;
		}
	}
	return false;
}

/*
	停止录制
*/
void CamerCtrl::StopRecordVideo()
{
	if (CheckDeviceOpend() == false) return ;
	if (m_videorecord.isOpened() && m_bRecordVideo) {
		m_videorecord.release();
		m_bRecordVideo = false;
	}
}

/*
	读取帧数据槽函数，读取之后 发送到主页面进行处理。
*/
void CamerCtrl::slot_timerout()
{
	bool bRet = false;
	cv::Mat frame, sendFrame;
	m_cap.read(frame);
	if (frame.empty())
		return;

	if(m_prevFrame.empty())
		m_prevFrame = frame;

	sendFrame = frame;

	// 检查是否需要人脸识别
	if (m_bFaceSwitch) {
		opencv_face(sendFrame);
	}
	// 时差是否需要物体移动识别
	else if (m_bMoveSwitch) {
		bRet = opencv_down_monitor(m_prevFrame, sendFrame);
	}

	emit signal_MatImage(sendFrame, bRet);
	m_prevFrame = frame; // 缓存原始帧数据
}

/*
	录制视频这里也要接收一些帧数据用来录制视频
*/
void CamerCtrl::slot_recordvideo(cv::Mat img, bool bRet)
{
	if (m_bRecordVideo) {
		m_videorecord.write(img);
	}
}

/*
	人脸检测
*/
void CamerCtrl::opencv_face(cv::Mat &image)
{
	// 加载一次即可
	if (m_bFaceLoaded == false) {
		m_bFaceLoaded = m_faceCascade.load("haarcascade_frontalface_default.xml");
	}
	if (m_bFaceLoaded == false) return;
	/*
	cv::CascadeClassifier faceCascade;
	if (!faceCascade.load("haarcascade_frontalface_default.xml")) {
		std::cerr << "Error loading face cascade classifier" << std::endl;
		return;
	}
	*/

	// 转换为灰度图像
	cv::Mat grayImage;
	cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);

	// 检测人脸
	std::vector<cv::Rect> faces;
	m_faceCascade.detectMultiScale(grayImage, faces, 1.1, 5, 0, cv::Size(30, 30)); // 这里的5 是调整参数 有的是3

	// 在图像中绘制矩形框来标识人脸
	for (size_t i = 0; i < faces.size(); i++) {
		cv::rectangle(image, faces[i], cv::Scalar(0, 0, 255), 2); // 2个像素的红色画红框
	}

	// 显示带有人脸标记的图像
	//cv::imshow("Faces found", image);
}

bool CamerCtrl::opencv_down_monitor(cv::Mat& prevFrame, cv::Mat& frame) {

	bool bRet = false;

	cv::Mat prevFrameGray;
	cv::Mat CurrentFrameGray;
	cv::Mat diff;

	// 转为灰度图片
	cv::cvtColor(prevFrame, prevFrameGray, cv::COLOR_BGR2GRAY);
	cv::cvtColor(frame, CurrentFrameGray, cv::COLOR_BGR2GRAY);

	// 计算帧差
	cv::absdiff(CurrentFrameGray, prevFrameGray, diff);
	cv::threshold(diff, diff, 30, 255, cv::THRESH_BINARY);

	// 寻找连通区域
	std::vector<std::vector<cv::Point>> contours;
	cv::findContours(diff.clone(), contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

	// 绘制连通区域
	for (size_t i = 0; i < contours.size(); i++) {
		if (cv::contourArea(contours[i]) > 30000) { // 这里的30000是调整参数
			bRet = true;
			cv::drawContours(frame, contours, static_cast<int>(i), cv::Scalar(0, 255, 0), 2); // 2个像素的绿色绘制绿框
		}
	}
	return bRet;
}
