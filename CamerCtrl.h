#pragma once

/*
	default 50fps
*/
#include <QObject>
#include <QCamera>
#include <QDir>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <QTimer>

class CamerCtrl : public QObject
{
	Q_OBJECT

public:
	CamerCtrl(QObject *parent);
	~CamerCtrl();
	
	void SetCamerSetting(int proid, double value);

	bool OpenCamer(QString strVideoPath);
	bool OpenCamer(int index);
	void StopCamera();

	bool StartRecordVideo();
	void StopRecordVideo();

	void SetFaceEnable(bool b) {
		m_bFaceSwitch = b;
		m_bMoveSwitch = !b;
	}

	void SetMoveMonitorEnable(bool b) {
		m_bMoveSwitch = b;
		m_bFaceSwitch = !b;
	}

private:
	bool CheckDeviceOpend();
signals:
	void signal_MatImage(cv::Mat img, bool bDownMonitor);
private slots:
	void slot_timerout();
	void slot_recordvideo(cv::Mat img, bool bRet);
private:
	cv::VideoCapture m_cap;
	cv::VideoWriter m_videorecord;

	QTimer* m_pTimer = nullptr;
	qreal m_dVideoFrame = 10;
	bool m_bCapture = false;
	bool m_bRecordVideo = false;
	int m_currentFPS = 30;

	cv::Mat m_prevFrame, diff;

	void opencv_face(cv::Mat& image);
	bool opencv_down_monitor(cv::Mat& prevFrame, cv::Mat& frame);
	cv::CascadeClassifier m_faceCascade;
	bool m_bFaceLoaded = false;

	bool m_bFaceSwitch = true;
	bool m_bMoveSwitch = false;
};
