#pragma once

#include <QWidget>
#include "ui_CVideoPlayer.h"
#include <QListWidgetItem>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QTime>
#include <QVector>
#include <QDir>

class CVideoPlayer : public QWidget
{
	Q_OBJECT

public:
	CVideoPlayer(QWidget *parent = nullptr, bool bServer = false);
	~CVideoPlayer();
	void ReflushDir();

private:
	void UpdateToUI();
private slots:
	void on_btn_play_clicked();
	void on_btn_pause_clicked();
	void on_btn_stop_clicked();

	void slot_dbclicked(QListWidgetItem* item);
	void slot_positionChanged(qint64 pos);
	void slot_getduration(qint64 duration);
private:
	Ui::CVideoPlayerClass ui;
	QVideoWidget* m_pPlayerWidget = nullptr;
	QMediaPlayer* m_player = nullptr;
	bool m_bServer = false;
	int m_nVoice = 50;

	QString m_strTotalFormattedTime;
	QString m_strCurrentFormattedTime;
	QStringList m_vecVideos;
};
